/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package lab.lab_03;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author peakk
 */
public class OXJUnitTest {
    
    public OXJUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void checkRow1(){
        char[][] table = {{'X','X','X'},{'-','-','-'},{'-','-','-'}};
        char turn = 'X';
        boolean result = OX.checkWin(table,turn);
        assertEquals(true,result);
    }
    @Test
    public void checkRow2(){
        char[][] table = {{'-','-','-'},{'X','X','X'},{'-','-','-'}};
        char turn = 'X';
        boolean result = OX.checkWin(table,turn);
        assertEquals(true,result);
    }
    @Test
    public void checkRow3(){
        char[][] table = {{'-','-','-'},{'-','-','-'},{'X','X','X'}};
        char turn = 'X';
        boolean result = OX.checkWin(table,turn);
        assertEquals(true,result);
    }
    @Test
    public void checkRow11(){
        char[][] table = {{'O','O','O'},{'-','-','-'},{'-','-','-'}};
        char turn = 'O';
        boolean result = OX.checkWin(table,turn);
        assertEquals(true,result);
    }
    @Test
    public void checkRow22(){
        char[][] table = {{'-','-','-'},{'O','O','O'},{'-','-','-'}};
        char turn = 'O';
        boolean result = OX.checkWin(table,turn);
        assertEquals(true,result);
    }
    @Test
    public void checkRow33(){
        char[][] table = {{'-','-','-'},{'-','-','-'},{'O','O','O'}};
        char turn = 'O';
        boolean result = OX.checkWin(table,turn);
        assertEquals(true,result);
    }
    @Test
    public void checkCol1(){
        char[][] table = {{'X','-','-'},{'X','-','-'},{'X','-','-'}};
        char turn = 'X';
        boolean result = OX.checkWin(table,turn);
        assertEquals(true,result);
    }
    @Test
    public void checkCol2(){
        char[][] table = {{'-','X','-'},{'-','X','-'},{'-','X','-'}};
        char turn = 'X';
        boolean result = OX.checkWin(table,turn);
        assertEquals(true,result);
    }
    @Test
    public void checkCol3(){
        char[][] table = {{'-','-','X'},{'-','-','X'},{'-','-','X'}};
        char turn = 'X';
        boolean result = OX.checkWin(table,turn);
        assertEquals(true,result);
    }
    @Test
    public void checkCol11(){
        char[][] table = {{'O','-','-'},{'O','-','-'},{'O','-','-'}};
        char turn = 'O';
        boolean result = OX.checkWin(table,turn);
        assertEquals(true,result);
    }
    @Test
    public void checkCol22(){
        char[][] table = {{'-','O','-'},{'-','O','-'},{'-','O','-'}};
        char turn = 'O';
        boolean result = OX.checkWin(table,turn);
        assertEquals(true,result);
    }
    @Test
    public void checkCol33(){
        char[][] table = {{'-','-','O'},{'-','-','O'},{'-','-','O'}};
        char turn = 'O';
        boolean result = OX.checkWin(table,turn);
        assertEquals(true,result);
    }
    @Test
    public void checkDownright(){
        char[][] table = {{'X','-','-'},{'-','X','-'},{'-','-','X'}};
        char turn = 'X';
        boolean result = OX.checkWin(table,turn);
        assertEquals(true,result);
    }
    @Test
    public void checkDownleft(){
        char[][] table = {{'-','-','X'},{'-','X','-'},{'X','-','-'}};
        char turn = 'X';
        boolean result = OX.checkWin(table,turn);
        assertEquals(true,result);
    }
    @Test
    public void checkDownright1(){
        char[][] table = {{'O','-','-'},{'-','O','-'},{'-','-','O'}};
        char turn = 'O';
        boolean result = OX.checkWin(table,turn);
        assertEquals(true,result);
    }
    @Test
    public void checkDownleft2(){
        char[][] table = {{'-','-','O'},{'-','O','-'},{'O','-','-'}};
        char turn = 'O';
        boolean result = OX.checkWin(table,turn);
        assertEquals(true,result);
    }   
    @Test
    public void testCheckdraw_false(){
        char[][] table = OX.gettable();
        boolean result = OX.checkDraw(table);
        assertEquals(false,result);

    }
}